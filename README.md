# Rundeck Vagrant Box Provisioned with Ansible

This project makes the assumptions that you have [Vagrant](https://www.vagrantup.com/), [VirtualBox](https://www.virtualbox.org/), and [Ansible](https://docs.ansible.com/ansible/latest/) installed. You don't need to know how to use Ansible, but it needs to be installed to provision the Vagrant box. If you're on macOS and have homebrew installed, Ansible can be installed with `brew install ansible`.

## Dependencies
* [Vagrant](https://www.vagrantup.com/)
* [VirtualBox](https://www.virtualbox.org/)
* [Ansible](https://docs.ansible.com/ansible/latest/)

## Getting Started
1. clone this repository
3. vagrant up (this will take some time to download the base vagrant box and provision)
9. from your browser, go to http://localhost:4440

## Details

By now, you should have Rundeck running port 4440. The magic happens because we told vagrant to map the VM's port 4440 to your computer's port 4440. If you have something else running on port 4440, then you might have a conflict. If you want to fire up your application on a different port, you'll need to add the mapping to the Vagrantfile and then run `vagrant reload`.

The VM that is created is based on Ubuntu 20.04 pulled from [Ubuntu Cloud Images](https://cloud-images.ubuntu.com/).
